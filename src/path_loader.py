# ***********************************************
# ***               PathLoader                ***
# ***-----------------------------------------***
# *** Elektro-potkan  <dev@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import importlib.util
import os
import sys


def load(name, path, base = None):
	'''Load module from specific file
	
	@param name - module name
	@param path - path to the file
	@param base - optional base of the path (usually the special constant __file__)
	@return loaded module
	'''
	
	if base:
		path = os.path.abspath(os.path.dirname(os.path.abspath(base)) + '/' + path)
	
	spec = importlib.util.spec_from_file_location(name, path)
	
	module = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(module)
	
	return module
# load


def pathInsert(path, base = None, force = False, i = 1):
	''' Inserts given path into python's search path
	
	@param path
	@param base - optional base of the path (usually the special constant __file__)
	@param force - True to skip duplicity check
	@param i - index to insert the path to
		None for appending it at the end
		default (1) is to insert it as the first one after the working directory
	'''
	
	if base:
		path = os.path.abspath(os.path.dirname(os.path.abspath(base)) + '/' + path)
	
	if path not in sys.path or force:
		if i == None:
			sys.path.append(path)
		else:
			sys.path.insert(i, path)
# pathInsert
