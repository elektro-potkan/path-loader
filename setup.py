# ***********************************************
# ***               PathLoader                ***
# ***-----------------------------------------***
# *** Elektro-potkan  <dev@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


from setuptools import find_packages, setup


setup(
	name = 'path-loader',
	author = 'Elektro-potkan',
	author_email = 'dev@elektro-potkan.cz',
	url = 'https://gitlab.com/elektro-potkan/path-loader',
	use_scm_version = True,
	description = 'Simple loading of modules from specific paths',
	license = 'GPLv3+',
	classifiers = [
		'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
		'Programming Language :: Python :: 3'
	],
	setup_requires = ['setuptools_scm'],
	packages = find_packages(where="src")
)
